function createNewUser() {

    const userName = prompt('Enter your name:');
    const userLastName = prompt('Enter your last name:');

    const newUser = {
        firstName: userName,
        lastName: userLastName,
        getLogin() {
            return (userName[0] + userLastName).toLowerCase();
        }
    };

    return newUser;
}

console.log(createNewUser().getLogin());